package br.com.southsystem.teste.southsystemteste.model;

public class Relatorio {
  private Integer quantidadeClientes;
  private Integer quantidadeVendedores;
  private Integer idVendaMaisCara;
  private Vendedor piorVendedor;

  public Integer getQuantidadeClientes() {
    return quantidadeClientes;
  }

  public void setQuantidadeClientes(Integer quantidadeClientes) {
    this.quantidadeClientes = quantidadeClientes;
  }

  public Integer getQuantidadeVendedores() {
    return quantidadeVendedores;
  }

  public void setQuantidadeVendedores(Integer quantidadeVendedores) {
    this.quantidadeVendedores = quantidadeVendedores;
  }

  public Integer getIdVendaMaisCara() {
    return idVendaMaisCara;
  }

  public void setIdVendaMaisCara(Integer idVendaMaisCara) {
    this.idVendaMaisCara = idVendaMaisCara;
  }

  public Vendedor getPiorVendedor() {
    return piorVendedor;
  }

  public void setPiorVendedor(Vendedor piorVendedor) {
    this.piorVendedor = piorVendedor;
  }

  @Override
  public String toString() {
    return "Quantidade de Clientes: \t" + quantidadeClientes + "\n" +
            "Quantidade de Vendedores: \t" + quantidadeVendedores + "\n" +
            "ID de Venda Mais Cara: \t" + idVendaMaisCara + "\n" +
            "Pior Vendedor: \t" + piorVendedor;
  }
}
