package br.com.southsystem.teste.southsystemteste.builders;

import br.com.southsystem.teste.southsystemteste.model.Cliente;
import br.com.southsystem.teste.southsystemteste.model.ResultadoImportacao;
import br.com.southsystem.teste.southsystemteste.model.Venda;
import br.com.southsystem.teste.southsystemteste.model.Vendedor;

import java.util.List;

public class ResultadoImportacaoBuilder {

  private ResultadoImportacao resultadoImportacao;

  ResultadoImportacaoBuilder() {
    resultadoImportacao = new ResultadoImportacao();
  }

  public ResultadoImportacaoBuilder vendedores(List<Vendedor> vendedores) {
    resultadoImportacao.setVendedores(vendedores);
    return this;
  }

  public ResultadoImportacaoBuilder nomeArquivo(String nomeArquivo) {
    resultadoImportacao.setNomeArquivo(nomeArquivo);
    return this;
  }

  public ResultadoImportacaoBuilder clientes(List<Cliente> clientes) {
    resultadoImportacao.setClientes(clientes);
    return this;
  }

  public ResultadoImportacaoBuilder vendas(List<Venda> vendas) {
    resultadoImportacao.setVendas(vendas);
    return this;
  }

  public ResultadoImportacao instance() {
    return this.resultadoImportacao;
  }

  public static ResultadoImportacaoBuilder of() {
    return new ResultadoImportacaoBuilder();
  }

}
