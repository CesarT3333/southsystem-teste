package br.com.southsystem.teste.southsystemteste.model;

public class Vendedor {
  private String cpf;
  private String nome;
  private Double salary;
  private Integer quantidadeVendas = 0;

  public Vendedor(String cpf, String nome, Double salary) {
    this.cpf = cpf;
    this.nome = nome;
    this.salary = salary;
  }

  public Integer getQuantidadeVendas() {
    return quantidadeVendas;
  }

  public void setQuantidadeVendas(Integer quantidadeVendas) {
    this.quantidadeVendas = quantidadeVendas;
  }

  public String getCpf() {
    return cpf;
  }

  public void setCpf(String cpf) {
    this.cpf = cpf;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public Double getSalary() {
    return salary;
  }

  public void setSalary(Double salary) {
    this.salary = salary;
  }

  @Override
  public String toString() {
    return "Vendedor{" +
            "cpf='" + cpf + '\'' +
            ", nome='" + nome + '\'' +
            ", salary=" + salary +
            '}';
  }
}