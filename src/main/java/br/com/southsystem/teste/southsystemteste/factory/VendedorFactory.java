package br.com.southsystem.teste.southsystemteste.factory;

import br.com.southsystem.teste.southsystemteste.model.Vendedor;

import java.util.Arrays;
import java.util.List;

public class VendedorFactory {

  public static Vendedor criaPorLInhaDat(String linhaDat) {

    List<String> linhaDatArray = Arrays.asList(linhaDat.split("ç"));

    return new Vendedor(
            linhaDatArray.get(1),
            linhaDatArray.get(2),
            Double.parseDouble(linhaDatArray.get(3))
    );
  }

}
