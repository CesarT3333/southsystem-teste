package br.com.southsystem.teste.southsystemteste.factory;

import br.com.southsystem.teste.southsystemteste.model.Cliente;
import br.com.southsystem.teste.southsystemteste.model.Vendedor;

import java.util.Arrays;
import java.util.List;

public class ClienteFactory {

  public static Cliente criaPorLinhaDat(String linhaDat) {
    List<String> linhaDatArray = Arrays.asList(linhaDat.split("ç"));

    return new Cliente(
            linhaDatArray.get(1),
            linhaDatArray.get(2),
            linhaDatArray.get(3)
    );
  }
}
