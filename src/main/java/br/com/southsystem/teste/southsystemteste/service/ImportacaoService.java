package br.com.southsystem.teste.southsystemteste.service;

import br.com.southsystem.teste.southsystemteste.config.FileStorageProperties;
import br.com.southsystem.teste.southsystemteste.exception.FileStorageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;
import org.apache.commons.io.FilenameUtils;

import java.nio.file.StandardCopyOption;
import java.net.MalformedURLException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.stereotype.Service;

@Service
public class ImportacaoService {

  private final Path fileStorageLocation;

  @Autowired
  public ImportacaoService(FileStorageProperties fileStorageProperties) {

    final String userHomeLocation = System.getProperty("user.home");

    this.fileStorageLocation = Paths.get(userHomeLocation + fileStorageProperties.getUploadDir())
            .toAbsolutePath().normalize();

    try {
      Files.createDirectories(this.fileStorageLocation);
    } catch (Exception ex) {
      throw new FileStorageException("Não foi possível criar o diretório de entrada dos arquivos => 'in'", ex);
    }
  }

  public String importaArquivos(MultipartFile file) {
    String fileName = StringUtils.cleanPath(file.getOriginalFilename());
    String fileExtension = FilenameUtils.getExtension(fileName);

    try {

      if (!fileExtension.equals("dat")) {
        throw new FileStorageException("Tipo de arquivo imnválido");
      }

      Path targetLocation = this.fileStorageLocation.resolve(fileName);
      Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

      return fileName;
    } catch (IOException ex) {
      throw new FileStorageException("Não foi possível importar o arquivo " + fileName, ex);
    }
  }

}
