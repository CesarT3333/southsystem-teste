package br.com.southsystem.teste.southsystemteste.service;

import br.com.southsystem.teste.southsystemteste.model.Relatorio;
import br.com.southsystem.teste.southsystemteste.model.ResultadoImportacao;
import br.com.southsystem.teste.southsystemteste.model.Venda;
import br.com.southsystem.teste.southsystemteste.model.Vendedor;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

@Service
public class GeradorRelatorioService {

  public void criaRelatorioAPartirDeResultadoImportacao(ResultadoImportacao resultadoImportacao) {
    Relatorio relatorio = new Relatorio();

    defineQuantidadeClientes(relatorio, resultadoImportacao);
    defineQuantidadeVendedores(relatorio, resultadoImportacao);
    defineVendaMaisCara(relatorio, resultadoImportacao);
    definePiorVendedor(relatorio, resultadoImportacao);

    geraRelatorioEmArquivo(relatorio, resultadoImportacao);
  }

  private void geraRelatorioEmArquivo(Relatorio relatorio, ResultadoImportacao resultadoImportacao) {
    BufferedWriter writer = null;
    final String userHomeLocation = System.getProperty("user.home");

    final String directoryOutputPath = userHomeLocation + "/data/out/" +
            resultadoImportacao.getNomeArquivo()
                    .replaceAll(".dat", "") + ".done.dat";
    try {
      writer = new BufferedWriter(new FileWriter(directoryOutputPath));
      writer.write(relatorio.toString());
      writer.close();
      System.out.println("Relatório gerado com sucesso \t" + directoryOutputPath);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }


  private void defineQuantidadeClientes(Relatorio relatorio, ResultadoImportacao resultadoImportacao) {
    relatorio.setQuantidadeClientes(resultadoImportacao.getClientes().size());
  }

  private void defineQuantidadeVendedores(Relatorio relatorio, ResultadoImportacao resultadoImportacao) {
    relatorio.setQuantidadeVendedores(resultadoImportacao.getVendedores().size());
  }

  private void defineVendaMaisCara(Relatorio relatorio, ResultadoImportacao resultadoImportacao) {
    resultadoImportacao.getVendas()
            .sort((venda1, venda2) -> {

              final Double totalItensVenda1 = venda1.getItenVenda().stream()
                      .map(i -> i.getItemPrice() * i.getItemQuantity()).reduce(
                              (valorItem1, valorItem2) -> valorItem1 + valorItem2).get();

              final Double totalItensVenda2 = venda2.getItenVenda().stream()
                      .map(i -> i.getItemPrice() * i.getItemQuantity()).reduce(
                              (valorItem1, valorItem2) -> valorItem1 + valorItem2).get();

              return totalItensVenda1.compareTo(totalItensVenda2);

            });

    Venda vendaMaisCara = resultadoImportacao.getVendas().get(resultadoImportacao.getVendas().size() - 1);
    relatorio.setIdVendaMaisCara(vendaMaisCara.getSaleID());

  }

  private void definePiorVendedor(Relatorio relatorio, ResultadoImportacao resultadoImportacao) {
    resultadoImportacao.getVendas().forEach(v -> {
      final String nomeVendedor = v.getSalesmanName();
      Vendedor vendedorDaVenda = resultadoImportacao.getVendedores().stream().findFirst().get();
      vendedorDaVenda.setQuantidadeVendas(vendedorDaVenda.getQuantidadeVendas() + 1);
    });

    resultadoImportacao.getVendedores().sort((vendedor1, vendedor2) ->
            vendedor1.getQuantidadeVendas().compareTo(vendedor2.getQuantidadeVendas()));

    relatorio.setPiorVendedor(resultadoImportacao.getVendedores().get(0));
  }

}
