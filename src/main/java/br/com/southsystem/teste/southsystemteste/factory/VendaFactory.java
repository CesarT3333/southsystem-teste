package br.com.southsystem.teste.southsystemteste.factory;

import br.com.southsystem.teste.southsystemteste.model.ItemVenda;
import br.com.southsystem.teste.southsystemteste.model.Venda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class VendaFactory {
  public static Venda criaPorLinhaDat(String linhaDat) {

    List<String> linhaDatArray = Arrays.asList(linhaDat.split("ç"));

    List<ItemVenda> itensVenda = Arrays.asList(linhaDatArray.get(2).split(","))
            .stream()
            .map(ItemVendaFactory::criaPorLinhaDat)
            .collect(Collectors.toList());

    return new Venda(
            Integer.parseInt(linhaDatArray.get(1)),
            itensVenda,
            linhaDatArray.get(3)
    );
  }
}
