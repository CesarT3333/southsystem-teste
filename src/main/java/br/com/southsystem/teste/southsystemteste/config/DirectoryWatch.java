package br.com.southsystem.teste.southsystemteste.config;

import br.com.southsystem.teste.southsystemteste.builders.ResultadoImportacaoBuilder;
import br.com.southsystem.teste.southsystemteste.exception.FileStorageException;
import br.com.southsystem.teste.southsystemteste.factory.ClienteFactory;
import br.com.southsystem.teste.southsystemteste.factory.VendaFactory;
import br.com.southsystem.teste.southsystemteste.factory.VendedorFactory;
import br.com.southsystem.teste.southsystemteste.model.Cliente;
import br.com.southsystem.teste.southsystemteste.model.ResultadoImportacao;
import br.com.southsystem.teste.southsystemteste.model.Venda;
import br.com.southsystem.teste.southsystemteste.model.Vendedor;
import br.com.southsystem.teste.southsystemteste.service.GeradorRelatorioService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ConfigurationProperties(prefix = "directorywatch")
public class DirectoryWatch {

  private final String ID_LINE_VENDEDOR = "001";
  private final String ID_LINE_CLIENTE = "002";
  private final String ID_LINE_VENDA = "003";

  @Autowired
  private GeradorRelatorioService geradorRelatorioService;

  DirectoryWatch() {
    iniciaListenerDeDiretorio();
  }

  private void iniciaListenerDeDiretorio() {

    final String userHomeLocation = System.getProperty("user.home");
    final String directoryInputPath = userHomeLocation + "/data/in";

    FileAlterationObserver observer = new FileAlterationObserver(directoryInputPath);
    FileAlterationMonitor monitor = new FileAlterationMonitor(1l);

    configuraListener(observer);
    monitor.addObserver(observer);

    try {
      monitor.start();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void processaConteudoArquivo(File file) {

    List<Vendedor> vendedores = new ArrayList<>();
    List<Cliente> clientes = new ArrayList<>();
    List<Venda> vendadas = new ArrayList<>();

    try {

      populaListaResultado(file, vendedores, clientes, vendadas);

      ResultadoImportacao resultadoImportacao =
              criaResultadoImportacao(file.getName(), vendedores, clientes, vendadas);

      geradorRelatorioService.criaRelatorioAPartirDeResultadoImportacao(resultadoImportacao);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private ResultadoImportacao criaResultadoImportacao(
          String nomeArquivo,
          List<Vendedor> vendedores,
          List<Cliente> clientes,
          List<Venda> vendadas
  ) {
    return ResultadoImportacaoBuilder.of()
            .nomeArquivo(nomeArquivo)
            .clientes(clientes)
            .vendas(vendadas)
            .vendedores(vendedores)
            .instance();
  }

  private void populaListaResultado(File file, List<Vendedor> vendedores, List<Cliente> clientes, List<Venda> vendadas) throws IOException {
    Arrays.asList(Files.readAllLines(file.toPath()).toArray(new String[0]))
            .forEach(i -> {

              if (i.contains(ID_LINE_VENDEDOR)) {
                vendedores.add(VendedorFactory.criaPorLInhaDat(i));
              } else if (i.contains(ID_LINE_CLIENTE)) {
                clientes.add(ClienteFactory.criaPorLinhaDat(i));
              } else if (i.contains(ID_LINE_VENDA)) {
                vendadas.add(VendaFactory.criaPorLinhaDat(i));
              }

            });
  }

  private void configuraListener(FileAlterationObserver observer) {
    observer.addListener(new FileAlterationListenerAdaptor() {
      @Override
      public void onFileCreate(File file) {
        validaExtensaoArquivo(file);
        processaConteudoArquivo(file);
      }

      @Override
      public void onFileChange(File file) {
        validaExtensaoArquivo(file);
        processaConteudoArquivo(file);
      }
    });
  }

  private void validaExtensaoArquivo(File file) {
    String fileName = StringUtils.cleanPath(file.getName());
    String fileExtension = FilenameUtils.getExtension(fileName);

    if (!fileExtension.equals("dat")) {
      throw new FileStorageException("Tipo de arquivo imnválido");
    }

  }

}