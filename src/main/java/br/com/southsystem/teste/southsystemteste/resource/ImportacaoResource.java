package br.com.southsystem.teste.southsystemteste.resource;

import br.com.southsystem.teste.southsystemteste.service.ImportacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/importacao")
public class ImportacaoResource {

  @Autowired
  private ImportacaoService importacaoService;

  @PostMapping
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void importaArquivos(@RequestParam("files") MultipartFile[] files) {
    Arrays.asList(files).forEach(importacaoService::importaArquivos);
  }

}
