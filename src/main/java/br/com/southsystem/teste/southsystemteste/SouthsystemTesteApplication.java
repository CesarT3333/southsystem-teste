package br.com.southsystem.teste.southsystemteste;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import br.com.southsystem.teste.southsystemteste.config.FileStorageProperties;
import br.com.southsystem.teste.southsystemteste.config.DirectoryWatch;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;

import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchService;
import java.nio.file.FileSystems;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.io.IOException;
import java.nio.file.Paths;
import java.nio.file.Path;

@SpringBootApplication
@EnableConfigurationProperties({FileStorageProperties.class, DirectoryWatch.class})
public class SouthsystemTesteApplication {
  public static void main(String[] args) {
    SpringApplication.run(SouthsystemTesteApplication.class, args);
  }
}

