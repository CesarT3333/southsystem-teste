package br.com.southsystem.teste.southsystemteste.model;

import java.util.List;

public class ResultadoImportacao {

  private List<Venda> vendas;
  private List<Vendedor> vendedores;
  private List<Cliente> clientes;
  private String nomeArquivo;

  @Override
  public String toString() {
    return "ResultadoImportacao{" +
            "vendas=" + vendas +
            ", vendedores=" + vendedores +
            ", clientes=" + clientes +
            '}';
  }

  public List<Venda> getVendas() {
    return vendas;
  }

  public void setVendas(List<Venda> vendas) {
    this.vendas = vendas;
  }

  public List<Vendedor> getVendedores() {
    return vendedores;
  }

  public void setVendedores(List<Vendedor> vendedores) {
    this.vendedores = vendedores;
  }

  public List<Cliente> getClientes() {
    return clientes;
  }

  public void setClientes(List<Cliente> clientes) {
    this.clientes = clientes;
  }

  public void setNomeArquivo(String nomeArquivo) {
    this.nomeArquivo = nomeArquivo;
  }

  public String getNomeArquivo() {
    return this.nomeArquivo;
  }
}
