package br.com.southsystem.teste.southsystemteste.model;

public class ItemVenda {

  private Integer itemID;
  private Integer itemQuantity;
  private Double itemPrice;

  public ItemVenda(Integer itemID, Integer itemQuantity, Double itemPrice) {
    this.itemID = itemID;
    this.itemQuantity = itemQuantity;
    this.itemPrice = itemPrice;
  }

  @Override
  public String toString() {
    return "ItemVenda{" +
            "itemID=" + itemID +
            ", itemQuantity=" + itemQuantity +
            ", itemPrice=" + itemPrice +
            '}';
  }

  public Integer getItemID() {
    return itemID;
  }

  public void setItemID(Integer itemID) {
    this.itemID = itemID;
  }

  public Integer getItemQuantity() {
    return itemQuantity;
  }

  public void setItemQuantity(Integer itemQuantity) {
    this.itemQuantity = itemQuantity;
  }

  public Double getItemPrice() {
    return itemPrice;
  }

  public void setItemPrice(Double itemPrice) {
    this.itemPrice = itemPrice;
  }
}
