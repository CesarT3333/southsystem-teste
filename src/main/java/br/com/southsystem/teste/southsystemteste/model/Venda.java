package br.com.southsystem.teste.southsystemteste.model;

import java.util.List;

public class Venda {

  private Integer saleID;
  private List<ItemVenda> itenVenda;
  private String salesmanName;

  public Venda(Integer saleID, List<ItemVenda> itenVenda, String salesmanName) {
    this.saleID = saleID;
    this.itenVenda = itenVenda;
    this.salesmanName = salesmanName;
  }

  @Override
  public String toString() {
    return "Venda{" +
            "saleID=" + saleID +
            ", itenVenda=" + itenVenda +
            ", salesmanName='" + salesmanName + '\'' +
            '}';
  }

  public Integer getSaleID() {
    return saleID;
  }

  public void setSaleID(Integer saleID) {
    this.saleID = saleID;
  }

  public List<ItemVenda> getItenVenda() {
    return itenVenda;
  }

  public void setItenVenda(List<ItemVenda> itenVenda) {
    this.itenVenda = itenVenda;
  }

  public String getSalesmanName() {
    return salesmanName;
  }

  public void setSalesmanName(String salesmanName) {
    this.salesmanName = salesmanName;
  }
}
