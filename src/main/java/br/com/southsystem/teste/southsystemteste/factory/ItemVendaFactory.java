package br.com.southsystem.teste.southsystemteste.factory;

import br.com.southsystem.teste.southsystemteste.model.ItemVenda;
import br.com.southsystem.teste.southsystemteste.model.Venda;

import java.util.Arrays;
import java.util.List;

public class ItemVendaFactory {
  public static ItemVenda criaPorLinhaDat(String linhaDat) {
    linhaDat = linhaDat
            .replaceAll("\\[", "")
            .replaceAll("\\]", "");

    List<String> linhaDatArray = Arrays.asList(linhaDat.split("-"));

    return new ItemVenda(
            Integer.parseInt(linhaDatArray.get(0)),
            Integer.parseInt(linhaDatArray.get(1)),
            Double.parseDouble(linhaDatArray.get(2))
    );
  }
}
